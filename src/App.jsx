import React, { Component } from 'react'
import Main from './components/main/Main'
import Nav from './components/nav/Nav'
export default class App extends Component {
  render() {
    return (
      <div>
        <Nav/>
        <Main/>
      </div>
    )
  }
}
