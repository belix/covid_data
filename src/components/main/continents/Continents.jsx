import React, { Component } from 'react'
import "./continents.css"
import unirest from 'unirest'

export default class Continents extends Component {
    constructor(props){
        super(props)
        this.state ={
            continents: []
        }
    }
    componentDidMount(){
        var req = unirest("GET", "https://corona.lmao.ninja/v2/continents?yesterday=true&sort")
        req.end(res => {
            if(res.error) console.log(res.error)
            this.setState({continents: JSON.parse(res.raw_body)})
        })
    }

    sortHandler = (e) => {
        var name = e.target.innerHTML.toLowerCase()
        console.log(name)
        if(name === "tests") this.setState({continents: this.state.continents.sort((a,b) => b.tests - a.tests)})
        if(name === "active") this.setState({continents: this.state.continents.sort((a,b) => b.active - a.active)})
        if(name === "critical") this.setState({continents: this.state.continents.sort((a,b) => b.critical - a.critical)})
        if(name === "cases") this.setState({continents: this.state.continents.sort((a,b) => b.cases - a.cases)})
        if(name === "deaths") this.setState({continents: this.state.continents.sort((a,b) => b.deaths - a.deaths)})
        if(name === "recovered") this.setState({continents: this.state.continents.sort((a,b) => b.recovered - a.recovered)})
    }

    sortHandlerToday = (e) => {
        var name = e.target.innerHTML.toLowerCase()
        if(name === "cases") this.setState({continents: this.state.continents.sort((a,b) => b.todayCases - a.todayCases)})
        if(name === "deaths") this.setState({continents: this.state.continents.sort((a,b) => b.todayDeaths - a.todayDeaths)})
        if(name === "recovered") this.setState({continents: this.state.continents.sort((a,b) => b.todayRecovered - a.todayRecovered)})
    }

    sortHandlerPerOneMillion = (e) => {
        var name = e.target.innerHTML.toLowerCase()
        if(name === "cases") this.setState({continents: this.state.continents.sort((a,b) => b.casesPerOneMillion - a.casesPerOneMillion)})
        if(name === "deaths") this.setState({continents: this.state.continents.sort((a,b) => b.deathsPerOneMillion - a.deathsPerOneMillion)})
        if(name === "recovered") this.setState({continents: this.state.continents.sort((a,b) => b.recoveredPerOneMillion - a.recoveredPerOneMillion)})
    }
    render() {
        return (
            <div id="continents">
                <h3>Continents</h3>
                <table>
                    <thead>
                    <tr className="disappearOnShrink">
                            <th colSpan="7">TOTAL</th>
                            <th colSpan="3">TODAY</th>
                            <th colSpan="3">PER 1 MILLION</th>
                        </tr>
                        <tr>
                            <td>Continent</td>
                            <td className="disappearOnShrink" onClick={this.sortHandler}>Tests</td>
                            <td className="disappearOnShrink" onClick={this.sortHandler}>Cases</td>
                            <td onClick={this.sortHandler}>Active</td>
                            <td className="disappearOnShrink" onClick={this.sortHandler}>Critical</td>
                            <td onClick={this.sortHandler}>Deaths</td>
                            <td onClick={this.sortHandler}>Recovered</td>
                            <td className="disappearOnShrink" onClick={this.sortHandlerToday}>Cases</td>
                            <td className="disappearOnShrink" onClick={this.sortHandlerToday}>Deaths</td>
                            <td className="disappearOnShrink" onClick={this.sortHandlerToday}>Recovered</td>
                            <td className="disappearOnShrink" onClick={this.sortHandlerPerOneMillion}>Cases</td>
                            <td className="disappearOnShrink" onClick={this.sortHandlerPerOneMillion}>Deaths</td>
                            <td className="disappearOnShrink" onClick={this.sortHandlerPerOneMillion}>Recovered</td>
                        </tr>                    
                    </thead>
                    <tbody>
                        {this.state.continents && this.state.continents.map( continent => (
                            <tr>
                                <td>{continent.continent}</td>
                                <td className="tests disappearOnShrink">{continent.tests.toLocaleString()}</td>
                                <td className="cases disappearOnShrink">{continent.cases.toLocaleString()}</td>
                                <td className="active">{continent.active.toLocaleString()}</td>
                                <td className="critical disappearOnShrink">{continent.critical.toLocaleString()}</td>
                                <td className="deaths">{continent.deaths.toLocaleString()}</td>
                                <td className="recovered">{continent.recovered.toLocaleString()}</td>
                                <td className="cases disappearOnShrink">{continent.todayCases.toLocaleString()}</td>
                                <td className="deaths disappearOnShrink">{continent.todayDeaths.toLocaleString()}</td>
                                <td className="recovered disappearOnShrink">{continent.todayRecovered.toLocaleString()}</td>
                                <td className="cases disappearOnShrink">{continent.casesPerOneMillion.toLocaleString()}</td>
                                <td className="deaths disappearOnShrink">{continent.deathsPerOneMillion.toLocaleString()}</td>
                                <td className="recovered disappearOnShrink">{continent.recoveredPerOneMillion.toLocaleString()}</td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        )
    }
}
