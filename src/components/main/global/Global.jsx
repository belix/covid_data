import React, { Component } from 'react'
import unirest from "unirest"
import "./global.css"

export default class Global extends Component {
    constructor(props){
        super(props);
        this.state = {
            global: {}
        }
    }
    componentDidMount(){
        var req = unirest("GET","https://corona.lmao.ninja/v2/all?yesterday")
        req.end(res => {
            if(res.error) console.log(res.error)
            console.log(JSON.parse(res.raw_body))
            this.setState({global: JSON.parse(res.raw_body)})
        })
    }
    render() {
        return (
            <div id="global">
                <h3>Global</h3>
                <table>
                    <thead>
                        <tr className="disappearOnShrink">
                            <th colSpan="8">TOTAL</th>
                            <th colSpan="3">TODAY</th>
                            <th colSpan="3">PER 1 MILLION</th>
                        </tr>
                        <tr>
                            <td>Location</td>
                            <td className="disappearOnShrink">Tests</td>
                            <td className="disappearOnShrink">Cases</td>
                            <td>Active</td>
                            <td className="disappearOnShrink">Critical</td>
                            <td>Deaths</td>
                            <td>Recovered</td>
                            <td id="affected" className="disappearOnShrink">Affected countries</td>
                            <td className="disappearOnShrink">Cases</td>
                            <td className="disappearOnShrink">Deaths</td>
                            <td className="disappearOnShrink">Recovered</td>
                            <td className="disappearOnShrink">Cases</td>
                            <td className="disappearOnShrink">Deaths</td>
                            <td className="disappearOnShrink">Recovered</td>
                        </tr>                    
                    </thead>
                    <tbody>
                        {this.state.global.cases && (
                            <tr>
                                <td>Global</td>
                                <td className="tests disappearOnShrink">{this.state.global.tests.toLocaleString()}</td>
                                <td className="cases disappearOnShrink">{this.state.global.cases.toLocaleString()}</td>
                                <td className="active">{this.state.global.active.toLocaleString()}</td>
                                <td className="critical disappearOnShrink">{this.state.global.critical.toLocaleString()}</td>
                                <td className="deaths">{this.state.global.deaths.toLocaleString()}</td>
                                <td className="recovered">{this.state.global.recovered.toLocaleString()}</td>
                                <td className="disappearOnShrink">{this.state.global.affectedCountries.toLocaleString()}</td>
                                <td className="cases disappearOnShrink">{this.state.global.todayCases.toLocaleString()}</td>
                                <td className="deaths disappearOnShrink">{this.state.global.todayDeaths.toLocaleString()}</td>
                                <td className="recovered disappearOnShrink">{this.state.global.todayRecovered.toLocaleString()}</td>
                                <td className="cases disappearOnShrink">{this.state.global.casesPerOneMillion.toLocaleString()}</td>
                                <td className="deaths disappearOnShrink">{this.state.global.deathsPerOneMillion.toLocaleString()}</td>
                                <td className="recovered disappearOnShrink">{this.state.global.recoveredPerOneMillion.toLocaleString()}</td>
                            </tr>
                        )}
                    </tbody>
                </table>
            </div>
        )
    }
}
