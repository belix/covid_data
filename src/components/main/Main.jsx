import React, { Component } from 'react'
import Continents from './continents/Continents'
import Countries from './countries/Countries'
import Global from './global/Global'
import "./main.css"

export default class Main extends Component {
    render() {
        return (
            <div id="main">
                <Global/>
                <Continents/>
                <Countries/>
            </div>
        )
    }
}
