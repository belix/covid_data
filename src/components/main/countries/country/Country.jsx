import React, { Component } from 'react'
import { Modal, ModalBody, ModalTitle } from 'react-bootstrap'
import ModalHeader from 'react-bootstrap/esm/ModalHeader'
import './country.css'

export default class Country extends Component {
    constructor(props){
        super(props)
        this.state = {}
    }
    render() {
        return (
            <div>       
                {this.props.country && (
                    <Modal animation={true} show={this.props.show} onHide={this.props.showHandler} dialogClassName="countryModal" size="lg">
                        <ModalHeader closeButton>
                            <ModalTitle as="h4">{this.props.country.country}</ModalTitle>
                        </ModalHeader>
                        <ModalBody as="div" style={{display:"flex", justifyContent:"center",alignContent:"center"}}>
                            <img src={this.props.country.countryInfo.flag} alt=" " id="countryFlag"/>
                            <div className="countryData">
                                <span>Today cases</span>{this.props.country.todayCases}<span></span>
                                <br/>
                                <span>Today deaths</span><span>{this.props.country.todayDeaths}</span>
                                <br/>
                                <span>Today recovered</span><span>{this.props.country.todayRecovered}</span>
                                <br/>
                                <span>Total tests</span><span>{this.props.country.tests}</span>
                                <br/>
                                <span>Total cases</span><span>{this.props.country.cases}</span>
                                <br/>
                                <span>Total active</span><span>{this.props.country.active}</span>
                                <br/>
                                <span>Total critical</span>{this.props.country.critical}<span></span>
                                <br/>
                                <span>Total deaths</span><span>{this.props.country.deaths}</span>
                                <br/>
                                <span>Total recovered</span><span>{this.props.country.recovered}</span>
                            </div>
                        </ModalBody>
                    </Modal> 
                )}
            </div>
        )
    }
}
