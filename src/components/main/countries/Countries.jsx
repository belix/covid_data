import React, { Component } from 'react'
import "./countries.css"
import unirest from 'unirest'
export default class Countries extends Component {
    constructor(props){
        super(props)
        this.state = {
            countries: [],
            filter: "",
        }
    }
    componentDidMount(){
        var req = unirest("GET","https://corona.lmao.ninja/v2/countries?yesterday&sort")
        req.end(res => {
            if(res.error) console.log(res.error)
            this.setState({countries:JSON.parse(res.raw_body)})
        })
    }
    sortHandler = (e) => {
        var name = e.target.innerHTML.toLowerCase()
        console.log(name)
        if(name === "tests") this.setState({countries: this.state.countries.sort((a,b) => b.tests - a.tests)})
        if(name === "active") this.setState({countries: this.state.countries.sort((a,b) => b.active - a.active)})
        if(name === "critical") this.setState({countries: this.state.countries.sort((a,b) => b.critical - a.critical)})
        if(name === "cases") this.setState({countries: this.state.countries.sort((a,b) => b.cases - a.cases)})
        if(name === "deaths") this.setState({countries: this.state.countries.sort((a,b) => b.deaths - a.deaths)})
        if(name === "recovered") this.setState({countries: this.state.countries.sort((a,b) => b.recovered - a.recovered)})
    }

    sortHandlerToday = (e) => {
        var name = e.target.innerHTML.toLowerCase()
        if(name === "cases") this.setState({countries: this.state.countries.sort((a,b) => b.todayCases - a.todayCases)})
        if(name === "deaths") this.setState({countries: this.state.countries.sort((a,b) => b.todayDeaths - a.todayDeaths)})
        if(name === "recovered") this.setState({countries: this.state.countries.sort((a,b) => b.todayRecovered - a.todayRecovered)})
    }

    sortHandlerPerOneMillion = (e) => {
        var name = e.target.innerHTML.toLowerCase()
        if(name === "cases") this.setState({countries: this.state.countries.sort((a,b) => b.casesPerOneMillion - a.casesPerOneMillion)})
        if(name === "deaths") this.setState({countries: this.state.countries.sort((a,b) => b.deathsPerOneMillion - a.deathsPerOneMillion)})
        if(name === "recovered") this.setState({countries: this.state.countries.sort((a,b) => b.recoveredPerOneMillion - a.recoveredPerOneMillion)})
    }
    
    filterHandler = (e) => {
        this.setState({filter: e.target.value.toLowerCase()})
        console.log(this.state.filter)
    }

    render() {
        return (
            <div id="countries">
                <h3>Countries</h3>
                <div className="searchbar">
                    <label htmlFor="filter">Filter: </label>
                    <input type="text" id="filter" placeholder="Filter.." onChange={this.filterHandler}/>
                    <i className="fa fa-search"></i>
                </div>
                <table>
                    <thead>
                        <tr className="disappearOnShrink">
                            <th colSpan="7">TOTAL</th>
                            <th colSpan="3">TODAY</th>
                            <th colSpan="3">PER 1 MILLION</th>
                        </tr>
                        <tr>
                            <td>Country</td>
                            <td className="disappearOnShrink" onClick={this.sortHandler}>Tests</td>
                            <td className="disappearOnShrink" onClick={this.sortHandler}>Cases</td>
                            <td onClick={this.sortHandler}>Active</td>
                            <td className="disappearOnShrink" onClick={this.sortHandler}>Critical</td>
                            <td onClick={this.sortHandler}>Deaths</td>
                            <td onClick={this.sortHandler}>Recovered</td>
                            <td className="disappearOnShrink" onClick={this.sortHandlerToday}>Cases</td>
                            <td className="disappearOnShrink" onClick={this.sortHandlerToday}>Deaths</td>
                            <td className="disappearOnShrink" onClick={this.sortHandlerToday}>Recovered</td>
                            <td className="disappearOnShrink" onClick={this.sortHandlerPerOneMillion}>Cases</td>
                            <td className="disappearOnShrink" onClick={this.sortHandlerPerOneMillion}>Deaths</td>
                            <td className="disappearOnShrink" onClick={this.sortHandlerPerOneMillion}>Recovered</td>
                        </tr>                    
                    </thead>
                    <tbody>
                        {this.state.countries && this.state.countries
                        .filter(country => country.country.toLowerCase().indexOf(this.state.filter) !== -1)
                        .map( country => (
                            <tr>
                                <td><img src={country.countryInfo.flag} alt=" " className="countryFlag"/>{country.country}</td>
                                <td className="tests disappearOnShrink">{country.tests.toLocaleString()}</td>
                                <td className="cases disappearOnShrink">{country.cases.toLocaleString()}</td>
                                <td className="active">{country.active.toLocaleString()}</td>
                                <td className="critical disappearOnShrink">{country.critical.toLocaleString()}</td>
                                <td className="deaths">{country.deaths.toLocaleString()}</td>
                                <td className="recovered">{country.recovered.toLocaleString()}</td>
                                <td className="cases disappearOnShrink">{country.todayCases.toLocaleString()}</td>
                                <td className="deaths disappearOnShrink">{country.todayDeaths.toLocaleString()}</td>
                                <td className="recovered disappearOnShrink">{country.todayRecovered.toLocaleString()}</td>
                                <td className="cases disappearOnShrink">{country.casesPerOneMillion.toLocaleString()}</td>
                                <td className="deaths disappearOnShrink">{country.deathsPerOneMillion.toLocaleString()}</td>
                                <td className="recovered disappearOnShrink">{country.recoveredPerOneMillion.toLocaleString()}</td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        )
    }
}
